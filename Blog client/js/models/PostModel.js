var attr = DS.attr;

App.Post = DS.Model.extend({
  title: attr('string'),
  author: attr('string'),
  date: attr('string'),
  excerpt: attr('string'),
  body: attr('string')
});