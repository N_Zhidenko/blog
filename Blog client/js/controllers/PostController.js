App.PostController = Ember.ObjectController.extend({
  isEditing: false,

  actions: {
    edit: function() {
      this.set('isEditing', true);
    },

    doneEditing: function() {
      this.set('isEditing', false);
      var post = this.get('model');
      post.save();
    },
    
    deleteRecord: function(model) {
      model.deleteRecord();
      model.save();
      this.transitionToRoute('posts');
    }
  } 
});