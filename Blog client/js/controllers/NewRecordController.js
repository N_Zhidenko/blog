App.NewRecordController = Ember.ObjectController.extend({
  actions: {
    save: function(model) {
      record = this.store.createRecord('post', {
        title: model.title,
        author: model.author,
        date: new Date(),
        excerpt: model.excerpt,
        body: model.body
      });
      record.save();
    } 
  }
});