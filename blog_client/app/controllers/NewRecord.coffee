App = require 'app'

###*
  Controller NewRecordController

  @class NewRecordController
  @namespace App
  @extends Ember.ObjectController
###
module.exports = App.NewRecordController = Ember.ObjectController.extend
  ###*
    Holds our model

    @property content
    @type Object
  ###
  content: null

  actions:
    save: () ->
      model = @get 'model'
      model = @get("store").createRecord("post",
        title: model.title
        author: model.author
        date: new Date
        excerpt: model.excerpt
        body: model.body
        )
      model.save()
      @transitionToRoute('post', model.id)
      return
