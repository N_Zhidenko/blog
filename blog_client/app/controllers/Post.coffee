App = require 'app'

###*
  Controller PostController

  @class PostController
  @namespace App
  @extends Ember.ObjectController
###
module.exports = App.PostController = Ember.ObjectController.extend
  isEditing: false
  actions:
    edit: ->
      @set "isEditing", true
      return

    doneEditing: ->
      @set "isEditing", false
      post = @get("model")
      post.save()
      return

    deleteRecord: (model) ->
      model.deleteRecord()
      model.save()
      @transitionToRoute "posts"
      return
  ###*
    Holds our model

    @property content
    @type Object
  ###
  content: null
