App = require 'app'

###*
  Model Post

  @class Post
  @namespace App
  @extends DS.Model
###
attr = DS.attr
module.exports = App.Post = DS.Model.extend
  title: attr "string"
  author: attr "string"
  date: attr "string"
  excerpt: attr "string"
  body: attr "string"


### If any special serializer or adapter is needed for this model, use this code:

App.PostAdapter = DS.RESTAdapter.extend
  # your adapter code here

App.PostSerializer = DS.RESTSerializer.extend
  # your serializer code here

###
