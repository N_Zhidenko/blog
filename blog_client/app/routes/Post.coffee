App = require 'app'

###*
  Route PostRoute

  @class PostRoute
  @namespace App
  @extends Ember.Route
###
module.exports = App.PostRoute = Ember.Route.extend
  ###*
    Override this if needed, else remove it

    @inheritDoc
  ###
  model: (params) ->
    @_super arguments...
    @modelFor("posts").findBy "id", params.post_id

  ###*
    Override this if needed, else remove it

    @inheritDoc
  ###
  setupController: (controller, model) ->
    @_super arguments...
