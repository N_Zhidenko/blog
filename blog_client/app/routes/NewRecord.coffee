App = require 'app'

###*
  Route NewRecordRoute

  @class NewRecordRoute
  @namespace App
  @extends Ember.Route
###
module.exports = App.NewRecordRoute = Ember.Route.extend
  ###*
    Override this if needed, else remove it

    @inheritDoc
  ###
  model: (params) ->
    # @_super arguments...
    model:
      title: ""
      author: ""
      excerpt: ""
      body: ""
  ###*
    Override this if needed, else remove it

    @inheritDoc
  ###
  setupController: (controller, model) ->
    @_super arguments...

 # @modelFor("posts").findBy "id", params.post_id