App = require 'app'

###*
  Route PostsRoute

  @class PostsRoute
  @namespace App
  @extends Ember.Route
###
module.exports = App.PostsRoute = Ember.Route.extend
  ###*
    Override this if needed, else remove it

    @inheritDoc
  ###
  model: (params) ->
    @_super arguments...
    @store.find "post"

  ###*
    Override this if needed, else remove it

    @inheritDoc
  ###
  setupController: (controller, model) ->
    @_super arguments...
