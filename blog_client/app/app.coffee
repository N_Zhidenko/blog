# Application bootstrapper

###*
  The application object

  @class App
  @extends Ember.Application
###
module.exports = App = Ember.Application.create
  ###*
    Name of the application

    @property name
    @type String
  ###
  name: 'Blog'

# App.ApplicationAdapter = DS.LSAdapter.extend(namespace: "app")
